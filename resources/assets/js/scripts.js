jQuery(document).ready(function($) {

  // fastclick (for mobile)
  $(function() {
    FastClick.attach(document.body);
  });

  var header_height = $(".header__inner").outerHeight();
  var banner_height = $(window).height() - header_height;

  // anchor scroll animation
  $(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          var scrollToPosition = $(target).offset().top - header_height - 30;
          $('html, body').animate({
            scrollTop: scrollToPosition
          }, 600);
          return false;
        }
      }
    });
  });

  // activity navigation
  if ($(".main").hasClass("main--activity")){
    $('body').scrollspy({ target: '#activity-nav', offset: header_height + 50 });
  }
  $(window).on('activate.bs.scrollspy', function (e) {
    history.replaceState({}, "", $("a[href^='#']", e.target).attr("href"));
  });


  // menu mobile
  $("#menu-toggle").click(function() {
    $(this).toggleClass("active");
    $(".header__nav--extended").slideToggle(500, "swing");
    return false;
  });
  $(document).keyup(function (e) {
    closeMenu();
  });
  $('.main').click(function(e) {
    closeMenu();
  });
  $('.nav__link--parent').click(function() {
    $parent = $(this);
    $sublist = $parent.parent('.nav__item').find('.nav__sublist');
    if (!$parent.hasClass('is-open')) {
      $parent.addClass('is-open');
      $sublist.slideDown(500, "swing");
      return false;
    } else {
      $parent.removeClass('is-open');
      $sublist.slideUp(500, "swing");
      return false;
    }
  });

  // sticky things on document.ready
  stickyThings();

});


$(window).resize(function() {
  if(this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function() {
    $(this).trigger('resizeEnd');
  }, 800);
});

$(window).bind('resizeEnd', function() {
  stickyThings();
});

function closeMenu() {
  $("#menu-toggle").removeClass("active");
  $(".header__nav--extended").slideUp(500, "swing");
}

function stickyThings() {
  // because of "header fixed"
  var header_height = $(".header__inner").outerHeight();
  var header_extended_height = $(".header__nav--extended").outerHeight();
  var window_height = $(window).height();
  var banner_height = $(window).height() - header_height;

  $(".banner").css("height", banner_height);
  $(".header__nav--extended").css("max-height", banner_height);

  // sticky
  $(".header__inner").stick_in_parent({
    parent: "body",
  });
  $(".banner").stick_in_parent({
    offset_top: header_height,
  });
  $(".activity__nav").stick_in_parent({
    parent: "main.main",
    offset_top: header_height,
  });

  // menu extended
  if (header_extended_height > window_height) {
    $(".header__nav--extended").addClass("overflow");
  } else {
    $(".header__nav--extended").removeClass("overflow");
  }
}
